<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Module_Blacklist extends Module {

    public $version = '1.1';

    public function info() {
        return array(
            'name' => array(
                'en' => 'Blacklist'
            ),
            'description' => array(
                'en' => 'PyroCMS module to blacklist ip addresses.'
            ),
            'frontend' => FALSE,
            'backend' => TRUE,
            'menu' => 'utilities',
            'shortcuts' => array(
                'create' => array(
                    'name' => 'blacklist:create',
                    'uri' => 'admin/blacklist/create',
                    'class' => 'add'
                ),
            )
        );
    }

    public function install() {
        $this->uninstall();

        $tables = array(
            'blacklist' => array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => '11',
                    'auto_increment' => TRUE,
                    'primary' => TRUE
                ),
                'ip' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '15'
                ),
                'reason' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '250',
                    'default' => ''
                ),
            )
        );


        if ($this->install_tables($tables)) {
            return TRUE;
        }
    }

    public function uninstall() {
        $this->dbforge->drop_table('blacklist'); {
            return TRUE;
        }
    }

    public function upgrade($old_version) {
        // Your Upgrade Logic
        if (version_compare($this->version, $old_version, '>')) {
            $field = array(
                'reason' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '250',
                    'default' => ''
                    ));
        }
        $this->dbforge->add_column('blacklist', $field);


        return TRUE;
    }

    public function help() {
        // Return a string containing help info
        // You could include a file and return it here.
        return "No documentation has been added for this module.<br />Contact the module developer for assistance.";
    }

}

/* End of file details.php */
