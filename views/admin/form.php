<section class="title">
    <h4><?php echo lang('blacklist:' . $this->method); ?></h4>
</section>

<section class="item">

    <?php echo form_open_multipart($this->uri->uri_string(), 'class="crud"'); ?>
    <fieldset>
        <div class="form_inputs">

            <ul>
                <li class="<?php echo alternator('', 'even'); ?>">
                    <label for="name"><?php echo lang('blacklist:ip'); ?> <span>*</span></label>
                    <div class="input"><?php echo form_input('ip', set_value('ip', $ip), 'class="width-15"'); ?></div>
                </li>
                <li class="<?php echo alternator('', 'even'); ?>">
                    <label for="name"><?php echo lang('blacklist:reason'); ?> <span>*</span></label>
                    <div class="input"><?php echo form_textarea('reason', set_value('reason', $reason), 'class="width-15"'); ?></div>
                </li>
            </ul>

        </div>

        <div class="buttons">
            <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))); ?>
        </div>
    </fieldset>
    <?php echo form_close(); ?>

</section>