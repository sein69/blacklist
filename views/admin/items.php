<section class="title">
	<h4><?php echo lang('blacklist:item_list'); ?></h4>
</section>

<section class="item">
	<?php echo form_open('admin/blacklist/delete');?>
    <fieldset>
	<?php if (!empty($items)): ?>
	
		<table>
			<thead>
				<tr>
					<th><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all'));?></th>
					<th><?php echo lang('blacklist:ip'); ?></th>
					<th><?php echo lang('blacklist:reason'); ?></th>
					<th></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<td colspan="5">
						<div class="inner"><?php $this->load->view('admin/partials/pagination'); ?></div>
					</td>
				</tr>
			</tfoot>
			<tbody>
				<?php foreach( $items as $item ): ?>
				<tr>
					<td><?php echo form_checkbox('action_to[]', $item->id); ?></td>
					<td><?php echo $item->ip; ?></td>
					<td><?php echo $item->reason; ?></td>
					<td class="actions">
						<?php echo
						anchor('admin/blacklist/edit/'.$item->id, lang('blacklist:edit'), 'class="button"').' '.
						anchor('admin/blacklist/delete/'.$item->id, lang('blacklist:delete'), array('class'=>'button confirm')); ?>
					</td>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		
		<div class="table_action_buttons">
			<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete'))); ?>
		</div>
		
	<?php else: ?>
		<div class="no_data"><?php echo lang('blacklist:no_items'); ?></div>
	<?php endif;?>
	</fieldset>
	<?php echo form_close(); ?>
</section>