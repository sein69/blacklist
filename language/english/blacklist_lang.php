<?php
//messages
$lang['blacklist:success']			=	'IP %s is now blacklisted';
$lang['blacklist:error']			=	'An error occured';
$lang['blacklist:no_items']                     =	'No blacklisted items';
$lang['blacklist:forbidden_msg']                =	'You don\'t have permission to view this site<br /><b>Reason:</b> %s';
$lang['blacklist:forbidden_title']                =	'Access forbidden';

//page titles
$lang['blacklist:create']			=	'Add IP to Blacklist';

//labels
$lang['blacklist:name']                         =	'Name';
$lang['blacklist:ip']                           =	'IP Address';
$lang['blacklist:reason']                       =	'Reason';
$lang['blacklist:slug']                         =	'Slug';
$lang['blacklist:manage']			=	'Manage';
$lang['blacklist:item_list']                    =	'Blacklisted IPs';
$lang['blacklist:view']                         =	'View';
$lang['blacklist:edit']                         =	'Edit';
$lang['blacklist:delete']			=	'Remove';

//buttons
$lang['blacklist:custom_button']                =	'Custom Button';
$lang['blacklist:items']			=	'Items';