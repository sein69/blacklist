# PyroCMS Blacklist Module

## Description

This module lets you blacklist any IP and add reason. It prevents user to view front-end site, and throws 403 forbidden http status.
Now You can even style forbidden message!

## Legal

Blacklist module was written by [Blazej Adamczyk](http://sein.com.pl/)

This work is licensed under the Creative Commons Attribution - No Derivative Works 3.0 Unported License. 
To view a copy of this license, visit http://creativecommons.org/licenses/by-nd/3.0/.

## Usage

To use this module simply download, unzip to addons/{site-ref}/modules and install via Extensions in your Control Panel.
Module is available under Utilities menu. You are free to add or change language files to fit your needs